+++
title = "Daily journal #2"
date = "2020-05-19"
author = "Zaha"
cover = "/images/toggl-day2.png"
tags = ["daily", "journal"]
description = "A good day :)"
showFullContent = false
+++

# How much did I work
Today I worked 4 hours, I didn't had toggl on when I was reading the problems (from now on I will). Also, I accumulated 3171 xp at C++ on Code::Stats (3168 xp -> 6339 xp).

# What did I do in the rest of the time
School and reading

# How much did I slept
Approximately 8 hours.

# What went well
I managed to work 4 hours and solve 2 problems. Also I had an idea to solve a pretty difficult problem but I didn't implemented it.

# What went bad
I did an CodeForces virtual contest ([Educational 66](https://codeforces.com/contest/1175)) but went really bad and solved 0 problems, I knew to do only a brute force for problem a which looks lie this:
```cpp
#include <iostream>
using namespace std;

int main() {
    int t;
    cin >> t;

    while (t--) {
        long long n, k;
        cin >> n >> k;

        int counter = 0;
        while (n) {
            if (!(n % k))
                n /= k;
            else
                --n;

            ++counter;
        }
        cout << counter << "\n";
    }
}
```

# What problems did I solved
[1146](https://gitlab.com/zaha/solved-problems-track/-/blob/master/Timus/1146/main.cpp) from [Timus](https://acm.timus.ru/problem.aspx?space=1&num=1146) and [2035](https://gitlab.com/zaha/solved-problems-track/-/blob/master/Timus/2035/main.cpp) from [Timus](https://acm.timus.ru/problem.aspx?space=1&num=2035).

# What problems did I attempted
[2030](https://gitlab.com/zaha/solved-problems-track/-/blob/master/Timus/2030/main.cpp) from [Timus](https://acm.timus.ru/problem.aspx?space=1&num=2030) but I stopped after brute force even I had a better idea to solve it.

# What could I do differently
I could solve others problems in the time I spent for 2030 from Timus.

# What are my plans for tomorrow
Idk what to expect, I want to complete my homeworks