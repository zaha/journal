+++
title = "Daily journal #0"
date = "2020-05-16"
author = "Zaha"
cover = ""
tags = ["others"]
description = "A new journey begins..."
showFullContent = false
+++

# A new journey begins
So, from Monday (2020-05-18) I'll start working very hard to achieve my scopes and will start updating this journal.

# What I will include?
I will include the following data to track my progress:
* what has gone well
* what has gone bad
* what I learned in that day
* time spent on phone
* time spent working
* probably time spent sleeping
* [Code::Stats](https://codestats.net/users/Zaha) xp updates
* what problems did I solved
* what problems I attempted
* what could I do differently
* what are my plans for tomorrow

Also, for weekly journal I will use [this template](https://docs.google.com/document/d/1RJLWQv5DAG3R5wGAfyr49uPjqbW23mynLU6f4hd3CBs/edit).
