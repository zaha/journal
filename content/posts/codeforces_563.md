+++
title = "CodeForces Round #563"
date = "2020-05-21"
author = "Zaha"
cover = "/images/submissions-codeforces_563.png"
tags = ["codeforces", "contests"]
description = "Am I disabled?"
showFullContent = false
+++

# How did my rating changed
The round was not live.

# What has gone well
I got instant ideas to solve problems A and B

# What has gone bad
I didn't find what was I doing wrong on problems B and C which resulted in 4 submissions whit verdict "Wrong answer".

# How much time I wrote code
* A: 10 minutes
* B: 15 minutes
* C: 5 minutes

# How much time I did debug
* A: 0 minutes
* B: 15 minutes before trying C then the rest of contest
* C: the rest of contest

# What could I do differently
I don't know

# Link to the contest
[Click me](https://codeforces.com/contest/1174)

# Codes

## A
```cpp
#include <iostream>
#include <set>
using namespace std;

multiset <int> nr;

int main() {
    int n;
    cin >> n;
    for (int i = 1; i <= 2*n; ++i) {
        int temp;
        cin >> temp;

        nr.emplace(temp);
    }

    int sum1 = 0, sum2 = 0;
    auto it = nr.begin();
    for (int i = 0; i < n; ++i) {
        sum1 += *it;
        ++it;
    }
    while (it != nr.end()) {
        sum2 += *it;
        ++it;
    }
    if (sum1 == sum2) {
        cout << "-1";
        return 0;
    }

    for (auto i = nr.begin(); i != nr.end(); ++i)
        cout << *i << " ";
    return 0;
}
```

## B
```cpp
#include <iostream>
#include <algorithm>
#include <vector>
#define ll long long
using namespace std;

vector <ll> arr;
struct {
    bool operator()(ll a, ll b) const {
        return (a < b) && ((a + b) & 1);
    }
} oddSum;

int main() {
    int n;
    cin >> n;
    for (int i = 1; i <= n; ++i) {
        ll temp;
        cin >> temp;

        arr.emplace_back(temp);
    }

    sort(arr.begin(), arr.end(), oddSum);
    for (auto i : arr)
        cout << i << " ";

    return 0;
}
```

## C
```cpp
#include <iostream>
#include <vector>
using namespace std;

vector <int> primes;

bool isPrime(int x) {
    for (auto i : primes)
        if (!(x % i))
            return false;

    primes.emplace_back(x);
    return true;
}

int main() {
    int n;
    cin >> n;

    int current = 1;
    for (int i = 2; i <= n; ++i)
        if (isPrime(i)) {
            cout << current << " ";
            ++current;
        } else
            cout << "1 ";

    return 0;
}
```