+++
title = "Daily journal #1"
date = "2020-05-18"
author = "Zaha"
cover = "/images/toggl-day1.png"
tags = ["daily", "journal"]
description = "A pretty bad day :("
showFullContent = false
+++

# How much did I work
First of all, my plans for today was to complete 6 hours of programing. Unfortunately I managed to work only approximately 4. In the cover are shown only 1:46 because I did 2 hours in the morning without realising i didn't use Toggl and Code::Stats. Speaking of, my Code::Stats shows me that I gained 3168 xp on C++ which brings me from level 0 to level 1 at C++ (0 xp -> 3168 xp).

# What did I do in the rest of the time
5 hours of school (4 of them unannounced) which ruined my plans. Also I played some GTA 5 (2 hours). I liked to do one more hour of programming after school but I felt too tired.

# How much did I slept
Only 6 hours, I don't know why I couldn't fall asleep.

# What went well
**Literally nothing for fuck's sake**

# What went bad
There is a whole list:
* I didn't sleep well;
* The school fucked my plans;
* I felt tired all the day;
* I didn't manage to solve any problems.

# What problems did I solved
None :(

# What problems did I attempted to solve
[Expresie](https://gitlab.com/zaha/solved-problems-track/-/blob/master/InfoArena/Expresie/main.cpp) from [InfoArena](https://www.infoarena.ro/problema/expresie) and [1654](https://gitlab.com/zaha/solved-problems-track/-/blob/master/Timus/1654/main.cpp) from [Timus](https://acm.timus.ru/problem.aspx?space=1&num=1654).

# What could I do differently
Do something to fall asleep faster. Also I could not participate to online classes but I don't think it's a good idea to do so cos I have only 3 weeks left of school so I should not fuck it up now.

# What are my plans for tomorrow
I wanna do at least 4 hours of programming, with a bit of luck I could fit another 2. Also in that time I wanna do a CodeForces virtual contest, as suggested.