+++
title = "Daily journal #3"
date = "2020-05-20"
author = "Zaha"
cover = "/images/toggl-day3.png"
tags = ["daily", "journal"]
description = "I hate school -_-"
showFullContent = false
+++

# How much did I work
Today I worked 2 hours and accumulated 1106 xp on Code::Stats (6339 xp -> 7445).

# What did I do in the rest of the time
Fucking homeworks.

# How much did I slept
8 to 9 hours.

# What went well
I solved 1224 very fast.

# What went bad
I tried to get a lower complexity on 1146 from Timus but I failed and I have no idea how to do it.

# What problems did I solved
[1224](https://gitlab.com/zaha/solved-problems-track/-/blob/master/Timus/1224/main.cpp) from [Timus](https://acm.timus.ru/problem.aspx?space=1&num=1224).

# What problems did I attempted
I attempted to get O(n^3) instead of O(n^4) on [1146](https://gitlab.com/zaha/solved-problems-track/-/blob/master/Timus/1146/main.cpp) from [Timus](https://acm.timus.ru/problem.aspx?space=1&num=1146).

# What could I do differently
Less homeworks?

# What are my plans for tomorrow
At least 4 hours of working and (maybe) configure OpenBSD on my ThinkPad.