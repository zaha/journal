---
title: "About"
date: 2020-03-22
---

# What is this?
This is a journal that I will update daily to track my progress on learning programming.

# Why?
This is just a motivation to keep coding and working hard. Sometimes I am a very lazy person so I need something to push me.

## My scopes
My main scope is to become a good programmer and a computer expert. This is a very hard scope to achieve.

Some other scopes that I want to achieve:
* Gain ability to solve advanced algorithmic problems
* Get a job at Discord
* Upgrade my setup
* Learn some hardware
* Ditch Google, Facebook etc
* Fully move to FOSS
* Ditch addiction for phone and gaming 

### My experience at moment of writing this:
* Advanced Linux and Windows experience
* Poor hardware experience
* Google ninja
* Basic C++ knowledge
* OK experience of solving beginners coding problems

### About me
So, I am a guy which is very passionate about technology in general. My friends call me a computer guru or a hacker. I don't see me like that. I see me like a very beginner which have sooo much to learn. 

### Actual setup (at moment of writing this)
* An laptop (Asus R510L) with the following specs:
    * X550LA motherboard
    * i5 4200U
    * 8gb DDR3
    * Integrated graphics card
    * 500 gb Samsung 860 Evo
    * 1 tb Baracuda 7200 rpm
    * 1360x768 display + external monitor (Akira TV, same resolution)