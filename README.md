# [Journal website](https://journal.zaha.ml/)
This is a journal of my progress in programing. See more on [About](https://journal.zaha.ml/about/) page.


## Requirements
* [hugo](https://gohugo.io/getting-started/installing) >= 57.0

## Locally running the journal
First, clone the repository:
```bash
git clone https://gitlab.com/zaha/coding-journal.git
cd coding-journal
```
Then, we need to run it:
```bash
hugo server -D
```
Note: You will also want to change `baseurl` in `config.toml`.